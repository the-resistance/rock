'use strict'

import fs from 'fs'
import Gun from 'gun'
import { Client, Collection, Intents, NewsChannel } from 'discord.js'
import { randomString } from './js/utils.js'
import { processMessage } from './js/ai.js'

const devChannel = process.env.DEVCHANNEL
const token = process.env.DISCORDTOKEN || null
const opts = {
    nickname: process.env.NICKNAME || 'Rock',
    discordChannel: process.env.DISCORDCHANNEL || 'the-fold',
    gunChannel: process.env.GUNCHANNEL || 'trade',
    gunRelay: process.env.GUNRELAY || 'https://resistance-relay.herokuapp.com/gun'
}

global.rock = {
    channel: null, 
    identifier: randomString(24)
}

const discordChannels = []
const subscribe = async (channel, relay, discordChannels) => {
    const gun = Gun({
        peers: [relay, 'https://59.thesource.fm/gun'],
        file: 'data',
        localStorage: false,
        radisk: true,
        axe: true
    })

    let messageCache = []
    let first = false
    global.rock.channel = gun.get('communications').get('channels').get(channel)
    global.rock.channel.on(async (node) => {
        if (!node.message || 
            !node.messageId || 
            !node.identifier ||
            node.messageId === undefined ||
            node.identifier === undefined ||
            node.message === undefined ||
            messageCache.includes(node.messageId)) {
            return
        }
        messageCache.push(node.messageId)
        if (messageCache.length >= 25) messageCache.shift()
        if (first === false) return first = true
        discordChannels.map((channel) => {
            if (devChannel == null) {
                if (channel.name !== devChannel) return
            }
            try {
                channel.send(node.message)
            }
            catch (err) {
                console.log(err)
            }
        })
    })
}

// Set up Discord bot
const createBot = async (opts) => {

    const client = new Client({ intents: [
            Intents.FLAGS.GUILDS, 
            Intents.FLAGS.GUILD_MESSAGES, 
            Intents.FLAGS.DIRECT_MESSAGES,
        ],
        partials: [
            'CHANNEL',
        ]
    })

    client.commands = new Collection()
    const commandFiles = fs.readdirSync('./src/js/cmds').filter(file => file.endsWith('.js'))

    for await (const file of commandFiles) {
        const command = await import(`./js/cmds/${file}`)
        client.commands.set(command.data.name, command)
    }
    
    const configureGuild = (guild) => {
        let found = false
        let first = false
        guild.me.setNickname(opts.nickname)
        guild.channels.cache.map((channel) => {
            if (found === true) return
            if (channel.name === opts.discordChannel) {
                discordChannels.push(channel)
                return found = true
            }
            if (first === false &&
                channel.type === 'GUILD_TEXT' && 
                channel.permissionsFor(client.user).has("VIEW_CHANNEL") === true &&
                channel.permissionsFor(client.user).has("SEND_MESSAGES") === true
            ) {
                return first = channel
            }
        })
        if (found !== true) discordChannels.push(first)
    } 

    client.once('ready', () => {
        client.guilds.cache.map((guild) => configureGuild(guild))
        subscribe(opts.gunChannel, opts.gunRelay, discordChannels)
        console.log('Ready to ROCK!')
    })

    client.on('guildCreate', guild => { 
        configureGuild(guild)
    })

    client.on('channelUpdate', (oldChannel, newChannel) => {
        const thisChannel = discordChannels.map(channel => channel.guild.id).indexOf(oldChannel.guild.id)
        if (thisChannel > -1) discordChannels.splice(thisChannel, 1)
        configureGuild(newChannel.guild)
    })

    client.on('interactionCreate', async interaction => {
        if (!interaction.isCommand()) return
        const command = client.commands.get(interaction.commandName)
        if (!command) return

        try {
            await command.execute(interaction)
        } 
        catch (error) {
            console.error(error)
            await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true })
        }
    })

    client.on('messageCreate', async (message) => {
        try {
            if(message.author.id === client.user.id) return
            if (message.channel.type !== 'DM') return
            await message.channel.send(await processMessage(message.content, {name: message.author.username, lowerDelay: 5000, upperDelay: 15000}))
        }
        catch (err) {
            console.log(err)
        }
    })

    client.login(token)
}

createBot(opts)