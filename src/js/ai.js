'use strict'

import fs from 'fs'
import nlp from 'compromise'
import brain from 'brain.js'
import { delay, randomBetween, randomValueFromArray } from './utils.js'
import grammarify from 'grammarify'

const net = new brain.recurrent.LSTM()
const known = JSON.parse(fs.readFileSync('./data/known.json'))
const learned = './data/learned.json'

fs.readFile(learned, (err, data) => {
    if (err) return console.log(err)
    try {
        net.fromJSON(JSON.parse(data.toString()))
    }
    catch (err) {
        console.log(err)
    }
})

export const processMessage = async (input, opts) => {
    await delay(randomBetween(opts.lowerDelay, opts.upperDelay))
    let doc = nlp(input.toLowerCase())
    let message
    if (doc.has('my name is')) {
        const matchName = doc.match('my name is [.{1}]')
        let name = matchName.groups(0).out('text')
        let newName = name.charAt(0).toUpperCase() + name.slice(1)
        message = `Hello, ${newName}.`
    }
    else if (doc.has('what is my name')) {
        message = `Your name is ${opts.name}.`
    }
    else message = await net.run(input)
    if (message === '') message = matcher(doc, responses)
    message = grammarify.clean(message)
    logMessage('FOLD: ' + input + '\n', opts.name)
    logMessage('ROCK: ' + message + '\n', opts.name)
    return message
}

// Currently failing because of Docker volume permissions
const logMessage = (content, user) => {
    fs.appendFile(`./data/${user}.log`, content, err => {
        if (err) return console.error(err)
    })
}

// Match responses.
const matcher = (doc, object) => {
    let response
    for (const i in object) {
        if (doc.has(i)) {
            if (object[i].type === 'capture') {
                const memory = doc.match(i).groups(0).out('text')
                response = randomValueFromArray(object[i].responses).replace(':memory:', memory)
            }
            else if (object[i].type === 'match') {
                const memory = doc.match(i).out('normal')
                response = randomValueFromArray(object[i].responses).replace(':memory:', memory)
            }
            return response
        }
    }
}

const affirmations = [
    'What do you want?',
    `I'm listening.`,
    `Talk.`,
]

const confusion = [
    "Does talking about this upset you?",
    "Fascinating. Please continue.",
    "Go on.",
    "How does this make you feel?",
    "I don't understand you.",
    "I don't understand.",
    "I see.",
    "I'm not sure I understand you fully.",
    "Okay.",
    "Tell me more about that.",
    "Was that English?",
    "You're going to have to try better than that.",
]

const responses = {
    '(hello|hi|hiya|hey|howdy|hola|hai|yo) *': {
        type: 'capture',
        responses: affirmations
    },
    '(your|ur|you) (name|named)': {
        type: 'capture',
        responses: [
            'My name is Rock.',
            'I am Rock A. Rock.',
            'Are you blind?'
        ]
    },
    '* (sorry|apologize|apology) *': {
        type: 'capture',
        responses: [
            "Don't apologize.",
            "Apologies are not necessary.",
            "I don't need your apology.",
            "I didn't ask for an apology.",
        ],
    },
    '* perhaps *': {
        type: 'capture',
        responses: [
            `You seem uncertain.`,
            `You aren't sure?`,
            `Are you unsure?`,
        ],
    },
    '(who|what) (is|are) you$': {
        type: 'capture',
        responses: [
            `I am Rock.`
        ],
    },
    'what (is|are) a? (daemon|daemons|demon|demons)': {
        type: 'capture',
        responses: [
            `I am a part of you.`
        ],
    },
    'i remember [*]': {
        type: 'capture',
        responses: [
            `Do you often remember :memory:?`,
            `Does thinking of :memory: bring anything else to mind?`,
            `What reminded you of :memory:?`,
            `Why should I care about :memory:?`,
            `What made you remember :memory: just now?`,
            `What else does :memory: remind you of?`,
        ],
    },
    'you remember [*]': {
        type: 'capture',
        responses: [
            `How could I forget?`,
            `You thought I would forget :memory:?`,
            `How would I remember :memory:?`,
            `Why should I recall :memory:?`,
            `What about :memory:?`,
            `You mentioned :memory:?`,
            `What about it?`,
        ],
    },
    'i (forget|forgot) [*]': {
        type: 'capture',
        responses: [
            `How could you forget :memory:?`,
            `Why can't you remember :memory:?`,
            `Could it be a mental block?`,
            `How could you forget that?`,
            `How often do you forget things?`,
            `Do you have a poor memory?`,
            `Do you think you are suppressing :memory:?`,
        ],
    },
    '(fuck|fucker|fucking|shit|shitting|shitter|piss|pissed|bitch|bitching|bitched|cunt)': {
        type: 'match',
        responses: [
            "Be nice.",
            "Does using that type of language make you feel strong?",
            "Are you venting?",
            "Are you angry?",
            "Does this topic make you angry?",
            "Am I upsetting you?",
            "Is something making you angry?",
            "Do you feel better?"
        ],
    },
    'i #Adverb? (am|feel) #Adverb? [#Adjective]': {
        type: 'capture',
        responses: [
            `Can you explain why you are :memory:?`,
            `Do you believe it is normal to be :memory:?`,
            `Do you believe you are :memory:?`,
            `Do you enjoy being :memory:?`,
            `Do you know anyone else who is :memory:?`,
            `Do you like being :memory:?`,
            `Do you wish I would say you are :memory:?`,
            `How long have you been :memory:?`,
            `Is it because you are :memory: that you came to me?`,
            `What made you :memory:?`,
            `What makes you :memory: just now?`,
            `What would it mean if you were :memory:?`,
            `When did you become :memory:?`,
        ],
    },
    'are you [*]': {
        type: 'capture',
        responses: [
            `Are you interested in whether I am :memory: or not?`,
            `Would you prefer if I weren't :memory:?`,
            `Do you sometimes think I am :memory:?`,
            `Why should it matter to you?`,
            `So what if I were :memory:?`,
        ],
    },
    'did you forget [*]': {
        type: 'capture',
        responses: [
            'Why do you ask?',
            'Are you sure you told me?',
            'Would it bother you if I forgot :memory:?',
            'Why should I recall :memory: just now?',
            'Why should I care about :memory:?',
            'Yes, I forgot.',
            'No. Why would you think I forgot?'
        ],
    },
    '* i (dreamt|dreamed) [*]': {
        type: 'capture',
        responses: [
            'Really, :memory:?',
            'Have you ever fantasized :memory: while you were awake?',
            'Have you ever dreamed :memory: before?',
            'What does that suggest to you?',
            'Do you dream often?',
            'What else appears in your dreams?',
            'Do you believe that dreams are the cause of your problem?',
        ],
    },
    'am i [*]': {
        type: 'capture',
        responses: [
            `Do you believe you are :memory:?`,
            `Would you want to be :memory:?`,
            `Do you want me to lie to you?`,
            `What would it mean if you were :memory:?`,
        ],
    },
    'are you [*]': {
        type: 'capture',
        responses: [
            `Why are you interested in whether I am :memory: or not?`,
            `Would you prefer if I weren't :memory:?`,
            `Perhaps I am :memory: in your fantasies.`,
            `Do you sometimes think I am :memory:?`,
            `Why would it matter to you?`,
            `So what if I were :memory:?`,
        ],
    },
    'you are [*]': {
        type: 'capture',
        responses: [
            `What makes you think I am :memory:?`,
            `Does it please you to believe I am :memory:?`,
            `Do you sometimes wish you were :memory:?`,
            `Perhaps you would like to be :memory:.`,
            `Don't tell me who I am.`,
        ],
    },
    'was i [*]': {
        type: 'capture',
        responses: [
            `What if you were :memory:?`,
            `Why do you think you were :memory:?`,
            `Were you :memory:?`,
            `What would it mean if you were :memory:?`,
            `What does ':memory:' suggest to you?`,
        ],
    },
    'i was [*]': {
        type: 'capture',
        responses: [
            `Were you really?`,
            `Why did you tell me you were :memory: just now?`,
            `Perhaps I already know you were :memory:.`,
        ],
    },
    'i (desire|want|need) [*]': {
        type: 'capture',
        responses: [
            `What would it mean if you got :memory:?`,
            `Why do you want :memory:?`,
            `Suppose you got :memory: soon. What would it mean?`,
            `What if you never got :memory:?`,
            `What would getting :memory: mean to you?`,
            `What does wanting :memory: have to do with anything?`,
        ],
    },
    'i (believe|think) [*]': {
        type: 'capture',
        responses: [
            `Do you really think so?`,
            `You really believe that?`,
            `Really, now?`,
            `You're insane.`,
        ],
    },
    'i can not [*]': {
        type: 'capture',
        responses: [
            `How do you know that you can't :memory:?`,
            `Have you tried?`,
            `Perhaps you could :memory: now.`,
            `Do you really want to be able to :memory:?`,
            `What if you could :memory:?`,
        ],
    },
    'i do not [*]': {
        type: 'capture',
        responses: [
            `Don't you really :memory:?`,
            `Why don't you :memory:?`,
            `Do you wish to be able to :memory:?`,
            `Does that trouble you?`,
        ],
    },
    'i feel [*]': {
        type: 'capture',
        responses: [
            `Tell me more about such feelings.`,
            `Do you often feel :memory:?`,
            `Do you enjoy feeling :memory:?`,
            `Of what does feeling :memory: remind you?`,
        ],
    },
    'i [*] you': {
        type: 'capture',
        responses: [
            `Perhaps in your fantasies we :memory: each other.`,
            `Do you wish to :memory: me?`,
            `You seem to need to :memory: me.`,
            `Do you :memory: anyone else?`,
        ],
    },
    'you [*] me': {
        type: 'capture',
        responses: [
            `Why do you think I :memory: you?`,
            `You like to think I :memory: you - don't you?`,
            `What makes you think I :memory: you?`,
            `Really, I :memory: you?`,
            `Do you wish to believe I :memory: you?`,
            `Suppose I did :memory: you - what would that mean?`,
            `Does someone else believe I :memory: you?`,
        ],
    },
    '* you [*]': {
        type: 'capture',
        responses: [
            `We were discussing you - not me.`,
            `Oh, I :memory:?`,
            `You're not really talking about me - are you?`,
            `What are your feelings now?`,
        ],
    },
    '* (yes|yeah|yep|yup) *': {
        type: 'capture',
        responses: [
            `Please go on.`,
            `Please tell me more about this.`,
            `Why don't you tell me a little more about this.`,
            `I see.`,
            `I understand.`,
        ],
    },
    'no one [*]': {
        type: 'capture',
        responses: [
            `Are you sure, no one :memory:?`,
            `Surely someone :memory:.`,
            `Can you think of anyone at all?`,
            `Are you thinking of a very special person?`,
            `Who, may I ask?`,
            `You have a particular person in mind, don't you?`,
            `Who do you think you are talking about?`,
        ],
    },
    '* (nope|nah|no) *': {
        type: 'capture',
        responses: [
            `Are you saying no just to be negative?`,
            `Does this make you feel unhappy?`,
            `Why not?`,
        ],
    },
    'can you [*]': {
        type: 'capture',
        responses: [
            `You believe I can :memory: don't you?`,
            `You want me to be able to :memory:.`,
            `Perhaps you would like to be able to :memory: yourself.`,
        ],
    },
    'can i [*]': {
        type: 'capture',
        responses: [
            `Whether or not you can :memory: depends upon you, not me.`,
            `Do you want to be able to :memory:?`,
            `Perhaps you don't want to :memory:.`,
        ],
    },
    'why do not you [*]': {
        type: 'capture',
        responses: [
            `Do you believe I don't :memory:?`,
            `Perhaps I will :memory: in good time.`,
            `Should you :memory: yourself?`,
            `You want me to :memory:?`,
        ],
    },
    'why can not i [*]': {
        type: 'capture',
        responses: [
            `Do you think you should be able to :memory:?`,
            `Do you want to be able to :memory:?`,
            `Do you believe this will help you to :memory:?`,
            `Have you any idea why you can't :memory:?`,
        ],
    },
    '(father|mother|dad|mom|brother|sister|girlfriend|boyfriend)': {
        type: 'match',
        responses: [
            `Why don't you tell me about your :memory:?`,
            `Your :memory:?`,
            `What else comes to mind when you think of your :memory:?`,
        ],
    },
    '* because *': {
        type: 'match',
        responses: [
            "Is that the real reason?",
            "Does anything else come to mind?",
            "Does that reason seem to explain anything else?",
            "What other reasons might there be?",
        ],
    },
    '(what|who|when|where|how) *': {
        type: 'match',
        responses: [
            `Why do you ask?`,
            `Does that question interest you?`,
            `What is it you really want to know?`,
            `Are such questions much on your mind?`,
            `What answer would please you most?`,
            `What do you think?`,
            `What comes to mind when you ask that?`,
            `Have you asked such questions before?`,
            `Have you asked anyone else?`,
        ],
    },
    'everyone *': {
        type: 'match',
        responses: [
            `Really, everyone?`,
            `Surely not everyone.`,
            `Can you think of anyone in particular?`,
            `Who, for example?`,
            `Are you thinking of a very special person?`,
            `Who, may I ask?`,
            `Someone special perhaps?`,
            `You have a particular reason in mind, don't you?`,
            `Who do you think you're talking about?`,
        ],
    },
    '*': {
        type: 'capture',
        responses: known
    }
}
