'use strict'

import fs from 'node:fs'
import { REST } from '@discordjs/rest'
import { Routes } from 'discord-api-types/v9'

const env = process.env.ENV || 'dev'
const task = process.env.TASK || 'deploy'
const commands = []
const commandFiles = fs.readdirSync('./src/js/cmds').filter(file => file.endsWith('.js'))
const creds = JSON.parse(fs.readFileSync('./config.json'))

for await (const file of commandFiles) {
	const command = await import(`./cmds/${file}`)
	commands.push(command.data.toJSON())
}

const rest = new REST({ version: '10' }).setToken(creds.token)

const deployer = async (env = 'dev', task = 'deploy') => {
	if (env === 'global') {
		if (task === 'deploy') {
			await rest.put(
				Routes.applicationCommands(creds.clientId),
				{ body: commands }
			)
		}
		else if (task === 'delete') {
			rest.get(Routes.applicationCommands(creds.clientId, creds.guildId))
			.then(data => {
				const promises = []
				for (const command of data) {
					const deleteUrl = `${Routes.applicationCommands(creds.clientId, creds.guildId)}/${command.id}`
					promises.push(rest.delete(deleteUrl))
				}
				return Promise.all(promises)
			})
		}

	}
	else if (env === 'dev') {
		if (task === 'deploy') {
			rest.put(Routes.applicationGuildCommands(creds.clientId, creds.guildId), { body: commands })
			.then(() => console.log('Successfully registered application commands.'))
			.catch(console.error)
		}
		else if (task === 'delete') {
			rest.get(Routes.applicationGuildCommands(creds.clientId, creds.guildId))
			.then(data => {
				const promises = []
				for (const command of data) {
					const deleteUrl = `${Routes.applicationGuildCommands(creds.clientId, creds.guildId)}/${command.id}`
					promises.push(rest.delete(deleteUrl))
				}
				return Promise.all(promises)
			})
		}
	}
	console.log(`Completed: ${task}, Environment: ${env}`)
}

deployer(env, task)