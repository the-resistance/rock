'use strict'

import { SlashCommandBuilder } from '@discordjs/builders'
import { randomString } from '../utils.js'
import grammarify from 'grammarify'

export const data = new SlashCommandBuilder()
	.setName('echo')
	.setDescription('Speak through me.')
	.addStringOption(option => option
		.setName('message')
		.setDescription('The message to send.')
		.setRequired(true)
	)

export const execute = async (interaction) => {
	const message = grammarify.clean(interaction.options.getString('message'))
	global.rock.channel.put({ message, identifier: global.rock.identifier, messageId: randomString(24) })
	return interaction.reply({ content: message, ephemeral: true })
}