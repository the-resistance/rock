'use strict'

export const delay = ms => new Promise(res => setTimeout(res, ms))

export const randomString = (len, chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') => {
    let text = ''
    for (let i = 0; i < len; i++) {
        text += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    return text
}

export const randomValueFromArray = (array) => {
    return array[Math.floor(Math.random()*array.length)]
}

export const randomBetween = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min)
}