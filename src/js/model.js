'use strict'

import fs from 'fs'
import brain from 'brain.js'

const net = new brain.recurrent.LSTM()

const train = async (array) => {
    console.log('Training...')
    const d = new Date()
    await net.train(array, {
        iterations: 20000,
        log: true,
        errorThresh: 0.01
    })
    fs.writeFile("./data/learned.json", JSON.stringify(net.toJSON()), (err) => {
        if (err)
          console.log(err)
        else {
            console.log(`Training finished in ${(new Date() - d) / 1000} s`)
        }
    })    
}

train(await JSON.parse(fs.readFileSync('./data/known.json')))