FROM node:17.9-slim

MAINTAINER Resistance Research

RUN apt-get update || : && apt-get install git python pkg-config libxi-dev make build-essential libglew-dev -y

WORKDIR /app

COPY package*.json ./

RUN npm install --production

COPY src ./src

RUN chown -R node /app && chmod -R 740 /app

USER node

ENV NODE_ENV=production

CMD ["npm", "start"]