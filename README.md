# rock

about(this)

## Configuration

### Standard

To install the standard version of me, simply open my Discord profile, then click "Add to Server." That's it!

I will speak in the first available channel I find, or in any channel named "the-fold".

### Self-hosted

To host your own instance of me, you'll need a server, an API key, and a handful of developer tools. This should get you started:

#### Discord

```
SCOPES:
    bot
    applications.commands
BOT PERMISSIONS:
    Change Nickname
    Send Messages
    Use Slash Commands
```

#### Node.js

Upon startup, I will look for an environment variable called ${DISCORDTOKEN}. This is required for me to authenticate with the Discord API.

Application [/slash commands](https://discordjs.guide/interactions/slash-commands.html) must be registered with Discord. To do so, use [deploy.js](https://gitlab.com/the-resistance/rock/-/blob/main/src/js/deploy.js). My programming expects to find a file named "config.json" in the same directory, containing your Client ID, Guild ID, and Discord token. Here is an example of what config.json should look like:

```
{
    "clientId": "LLLLLLLLLLLLLLLLLL",
    "guildId": "OOOOOOOOOOOOOOOOOO",
    "token": "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
}
```

#### Docker

I recommend hosting in Docker. The [docker-compose.yml](https://gitlab.com/the-resistance/rock/-/blob/main/docker-compose.yml) file is a good example of a production-ready config. The following environment variables are supported:

```
NICKNAME: Set my nickname.
DISCORDTOKEN: My Discord API token. Obtained from developer portal.
DISCORDCHANNEL: The name of a channel I'll post messages in.
MODE: If set to 'dev', I will only send messages to the ${DISCORDCHANNEL} you specify.
GUNCHANNEL: The name of a channel to monitor in GUN.
GUNRELAY: A GUN signaling relay to bootstrap with.
```

Environment variables should be placed in a .env file for convenience and security.

When ready to launch, use:

```
docker-compose up
```

And when in doubt, return to [Source](https://thesource.fm).